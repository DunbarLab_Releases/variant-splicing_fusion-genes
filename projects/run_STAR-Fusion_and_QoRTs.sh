#!/bin/sh

# sbatch --partition=norm --cpus-per-task=32 --mem=200g --time=48:00:00

/home/cordessf/scripts/variant-splicing/shell/STAR-Fusion_and_QoRTs_pipeline.sh -m "/data/DunbarGroup/ZL34/FASTQ/CD34+/metadata.txt" -o /scratch/stefan/ZL34_CD34 -p 0
