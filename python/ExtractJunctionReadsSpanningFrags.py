#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  5 16:44:10 2018

@author: cordessf
"""

import gzip


'''
#############################################
# extract_junction_reads_and_spanning_frags #
#############################################
'''
def extract_junction_reads_and_spanning_frags(base_results_dir,
                                               metadata_file):
    # Parse the metadata file
    paths = dict()
    samples = list()
    with open(metadata_file) as metadata_stream:
        line_count = 1
        for line in metadata_stream:
            fields = line.strip().split('\t')
            if (line_count == 1):
                for i in range(len(fields)):
                    if (fields[i] == "sample"):
                        sample_field = i
                    elif (fields[i] == "path"):
                        path_field = i
            else:
                s = fields[sample_field]
                
                paths[s] = fields[path_field]
                samples.append(s)
            line_count += 1

    # Set up lists and dictionaries across all samples
    fusion_fragments_per_million = dict()
    fusion_name = dict()
    junction_read_count = dict()
    junction_reads = dict()
    left_break_dinucl = dict()
    left_break_point = dict()
    left_break_entropy = dict()
    right_break_dinuc = dict()
    right_break_point = dict()
    right_break_entropy = dict()
    spanning_frag_count = dict()
    spanning_frags = dict()
    for s in samples:
        star_fusion_results = base_results_dir + "/STAR-Fusion_results/" + s + \
                                "/star-fusion.fusion_predictions.tsv"
        print("Extracting junction reads and spanning frags from: " + \
              star_fusion_results + "\n")
        
        # Set up per sample lists and dictionaries
        sample_fusion_fragments_per_million = dict()
        sample_fusion_name = dict()
        sample_junction_read_count = dict()
        sample_junction_reads = dict()
        sample_left_break_dinucl = dict()
        sample_left_break_entropy = dict()
        sample_left_break_point = dict()
        sample_right_break_dinuc = dict()
        sample_right_break_point = dict()
        sample_right_break_entropy = dict()
        sample_spanning_frag_count = dict()
        sample_spanning_frags = dict()
        with open(star_fusion_results) as star_fusion_results_stream:
            line_count = 1
            for line in star_fusion_results_stream:
                fields = line.strip().split('\t')
                if (line_count == 1):
                    # Parse the header
                    for i in range(len(fields)):
                        if (fields[i] == "#FusionName"):
                            fusion_name_field = i
                        elif (fields[i] == "JunctionReadCount"):
                            junction_read_count_field = i
                        elif (fields[i] == "SpanningFragCount"):
                            spanning_frag_count_field = i
                        elif (fields[i] == "LeftBreakpoint"):
                            left_break_point_field = i
                        elif (fields[i] == "RightBreakpoint"):
                            right_break_point_field = i
                        elif (fields[i] == "JunctionReads"):
                            junction_reads_field = i
                        elif (fields[i] == "SpanningFrags"):
                            spanning_frags_field = i
                        elif (fields[i] == "FFPM"):
                            fusion_frags_per_mill_field = i
                        elif (fields[i] == "LeftBreakDinuc"):
                            left_break_dinuc_field = i
                        elif (fields[i] == "LeftBreakEntropy"):
                            left_break_entropy_field = i
                        elif (fields[i] == "RightBreakDinuc"):
                            right_break_dinuc_field = i
                        elif (fields[i] == "RightBreakEntropy"):
                            right_break_entropy_field = i
                        else:
                            print("Unrecognized header field:" + \
                                  fields[i] + '\n')
                    line_count += 1
                else:
                    # Accumulate data
                    
                    lbp = fields[left_break_point_field]
                    rbp = fields[right_break_point_field]
                    key = lbp + "::" + rbp

                    sample_fusion_fragments_per_million[key] = \
                        fields[fusion_frags_per_mill_field]
                    sample_fusion_name[key] = \
                        fields[fusion_name_field]
                    sample_junction_read_count[key] = \
                        fields[junction_read_count_field]
                    
                    # Parse junction reads
                    jr = fields[junction_reads_field].split(',')
                    sample_junction_reads[key] = jr
                    
                    sample_left_break_dinucl[key] = \
                        fields[left_break_dinuc_field]
                    sample_left_break_point[key] = \
                        fields[left_break_point_field]
                    sample_left_break_entropy[key] = \
                        fields[left_break_entropy_field]
                    sample_right_break_dinuc[key] = \
                        fields[right_break_dinuc_field]
                    sample_right_break_entropy[key] = \
                        fields[right_break_entropy_field]
                    sample_right_break_point[key] = \
                        fields[right_break_point_field]
                    sample_spanning_frag_count[key] = \
                        fields[spanning_frag_count_field]
                    
                    # Parse spanning fragments
                    sf = fields[spanning_frags_field].split(',')
                    sample_spanning_frags[key] = sf
                    
        
        # Copy per sample results back to overall results        
        fusion_fragments_per_million[s] = sample_fusion_fragments_per_million
        fusion_name[s] = sample_fusion_name
        junction_read_count[s] = sample_junction_read_count
        junction_reads[s] = sample_junction_reads
        left_break_dinucl[s] = sample_left_break_dinucl
        left_break_point[s] = sample_left_break_point
        left_break_entropy[s] = sample_left_break_entropy
        right_break_dinuc[s] = sample_right_break_dinuc
        right_break_point[s] = sample_right_break_point
        right_break_entropy[s] = sample_right_break_entropy
        spanning_frag_count[s] = sample_spanning_frag_count
        spanning_frags[s] = sample_spanning_frags
        
    # Read through the bam files to find the actual junction reads and
    # spanning fragments
    junction_read_seq_1 = dict()
    junction_read_seq_2 = dict()
    spanning_frag_seq_1 = dict()
    spanning_frag_seq_2 = dict()
    for s in samples:
        r1_fastq_file = paths[s] + "/" + s + "_R1_001.fastq.gz"
        r2_fastq_file = paths[s] + "/" + s + "_R2_001.fastq.gz"
        print("\n\nExtracting junction reads and spanning frags from: " + \
              r1_fastq_file + "\n")
        
        # Target QNAMES
        targets_for_jr = junction_reads[s]
        targets_for_sf = spanning_frags[s]
        
        # Set up per sample lists
        junction_read_to_fusion_name = dict()
        targets_for_jr = list()
        sample_junction_read_seq_1 = dict()
        sample_junction_read_seq_2 = dict()
        for key in fusion_name[s]:
            sample_junction_read_seq_1[key] = dict()
            sample_junction_read_seq_2[key] = dict()
            for r in junction_reads[s][key]:
                junction_read_to_fusion_name[r] = key
                targets_for_jr.append(r)

        spanning_frag_to_fusion_name = dict()
        targets_for_sf = list()
        sample_spanning_frag_seq_1 = dict()
        sample_spanning_frag_seq_2 = dict()
        for key in fusion_name[s]:
            sample_spanning_frag_seq_1[key] = dict()
            sample_spanning_frag_seq_2[key] = dict()
            for r in spanning_frags[s][key]:
                spanning_frag_to_fusion_name[r] = key
                targets_for_sf.append(r)
                        
        # Open the FASTQ file
        with gzip.open(r1_fastq_file) as r1_stream:
            with gzip.open(r2_fastq_file) as r2_stream:
                jr_target_line = 0
                line_count = 0
                sf_target_line = 0
                for r1, r2 in zip( r1_stream, r2_stream ):
                    line_count += 1
                    
                    if line_count % 40000 == 0:
                        print(" " + str(line_count//4) + ".", end = "")
                        
                        
                    if line_count % 4 == 1:
                        # Split the read 1 and read 2 comment lines
                        [ qn_1, tag_1 ] = \
                            r1.decode('ascii').strip().split(" ")
                        [ qn_2, tag_2 ] = \
                            r2.decode('ascii').strip().split(" " )
                        qn_1 = qn_1[1:]
                        qn_2 = qn_2[1:]
                        
                        if (qn_1 != qn_2):
                            print("WARNING: qname mismatch on line " +\
                                  line_count + "\n")
        
                        if qn_1 in targets_for_jr:
                            jr_target_line = 1
                        elif qn_1 in targets_for_sf:
                            sf_target_line = 1
                            
                    elif line_count % 4 == 2:
                        seq_1 = r1.decode('ascii').strip()
                        seq_2 = r2.decode('ascii').strip()
                        
                        if jr_target_line == 1:
                            print("*", end = "")
                            key = junction_read_to_fusion_name[qn_1]
                            sample_junction_read_seq_1[key][qn_1] = seq_1
                            sample_junction_read_seq_2[key][qn_2] = seq_2
                        elif sf_target_line == 1:
                            print("*", end = "")
                            key = spanning_frag_to_fusion_name[qn_1]
                            sample_spanning_frag_seq_1[key][qn_1] = seq_1
                            sample_spanning_frag_seq_2[key][qn_2] = seq_2
                            
                    elif line_count % 4 == 0:
                        jr_target_line = 0
                        sf_target_line = 0
        
        # Copy per sample results back to the overall results
        junction_read_seq_1[s] = sample_junction_read_seq_1
        junction_read_seq_2[s] = sample_junction_read_seq_2
        spanning_frag_seq_1[s] = sample_spanning_frag_seq_1
        spanning_frag_seq_2[s] = sample_spanning_frag_seq_2
        
    final_report_stream = open(base_results_dir + \
                               "/JunctionRead_SpanningFrag_Report.txt", "w")
        
    # Generate the final report
    for s in samples:
        for key, fn in fusion_name[s].items():
            header = \
"""
\n
===============================================================================
Fusion Name: {:s}
Junction Read Counts: {:s}
Left Break Point: {:s}
Left Break Dinucleotides: {:s}
Right Break Point: {:s}
Right Break Dinucleotides: {:s}""".format(fn, junction_read_count[s][key], \
                                          left_break_point[s][key], \
                                          left_break_dinucl[s][key],
                                          right_break_point[s][key], \
                                          right_break_dinuc[s][key] )
            final_report_stream.write(header)
            
            # Loop over the junction reads
            header = \
"""
-------------------------------------------------------------------------------
Junction Reads
-------------------------------------------------------------------------------
QName\t\tRead 1\t\tRead 2"""
            final_report_stream.write(header)

            for r in junction_reads[s][key]:
                try:
                    read_line = \
                    """
{:s}\t\t{:s}\t\t{:s}""".format(r, \
           junction_read_seq_1[s][key][r], \
           junction_read_seq_2[s][key][r])
                    final_report_stream.write(read_line)

                except OSError:
                    print("No junction read data for " + s + "\t" + key + "\t" + r)
                except:
                    print("No junction read data for " + s + "\t" + key + "\t" + r)
                
            # Loop over the spanning frags
            header = \
"""
-------------------------------------------------------------------------------
Spanning Frags
-------------------------------------------------------------------------------
QName\t\tPos\t\tSequence"""
            final_report_stream.write(header)

            for r in spanning_frags[s][key]:
                try:
                    read_line = \
"""
{:s}\t\t{:s}\t\t{:s}""".format(r, \
                               spanning_frag_seq_1[s][key][r], \
                               spanning_frag_seq_2[s][key][r])
                    final_report_stream.write(read_line)
                    
                except OSError:
                    print("No spanning frag data for " + s + "\t" + key + "\t" + r)
                except:
                    print("No spanning frag data for " + s + "\t" + key + "\t" + r)
                    
    # Close report file
    final_report_stream.close()
                
                
    return [fusion_fragments_per_million, fusion_name, \
            junction_read_count, junction_reads, \
            junction_read_seq_1, junction_read_seq_2, \
            left_break_dinucl, left_break_point, left_break_entropy, \
            right_break_dinuc, right_break_point, right_break_entropy, \
            spanning_frag_count, spanning_frags, \
            spanning_frag_seq_1, spanning_frag_seq_2]
        
[ff, fn, jrc, jr, jrp, jrs, lbd, lbp, lbe, rbd, rbp, rbe, sfc, sf, sfp, sfs] = \
     extract_junction_reads_and_spanning_frags(base_results_dir = "/Volumes/passport-2/dunbar-data/ZL34_CD34", \
                                          metadata_file = "/Volumes/passport-2/dunbar-data/ZL34_CD34/FASTQ/CD34+/metadata.txt")