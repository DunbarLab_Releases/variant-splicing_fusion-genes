#!/bin/bash
# run_STAR-Fusion.sh

programname=`basename $0`
version=0.1

# Ensure that the entire pipeline fails, if it encouters an error at an intermediate step
set -o pipefail
set -e

# ================= DEFINE SOME CONVENIENCE FUNCTIONS =========================================================================
# ... Function to print informative error message upon fail
function fail {
        echo "$@" >&2
        exit 1
}

# ... Function to parse variables from configuration file
function parse_config () {
    awk -v section="$2" -v variable="$3" '
        $0 == "[" section "]" { in_section = 1; next }
        in_section && $1 == variable {
            $1=""
            $2=""
            sub(/^[[:space:]]+/, "")
            print
            exit
        }
        in_section && $1 == "" {
            # we are at a blank line without finding the var in the section
            print "Variable not found in configuration file." > "/dev/stderr"
            exit 1
        }
    ' "$1"
}

# ... Function to provide help on usage
function usage () {
echo -e "\e[0;35m
USAGE:

  $progname -m <metadata file> -o <output dir> [-e] [-h] [-p <phase>]

Perform single cell SCI-RNA-Seq tagging, trimming, alignment and digital gene expression matrix creation.

  -a <test_formula_1>  : Alternate hypothesis model used in hypothesis testing.  (Optional.)
  -e                   : Echo commands instead of executing them (for debugging purposes)
  -g <gene_level_form> : Base formula used to estimate total gene-level expression.  (Optional.)
  -h                   : Help.  Offers guidance on usage of this shell script. (Optional.)
  -m <metadata file>   : Full path to the metadata file.  (Required.)
  -n <test_formula_0>  : Null hypothesis model used in hypothesis testing.  (Optional.)
  -o <output dir>      : Where to write output bam.  Default: current directory. (Required.)
  -p <phase>           : Phase on which to start pipeline. (Optional.)
  -s <effect_formula>  : Base formula for the model used for effect size estimation.  (Optional.)
\e[0m"
}

# ================= SET DEFAULT VALUES ========================================================================================
alt_hypoth="~ sample + countbin + condition : countbin"
echo_prefix=""
effect_hypoth="~ condition + countbin + condition : countbin"
gene_hypoth="~ condition"
null_hypoth="~ sample + countbin"
phase=0

# ================= EXTRACT VARIABLES FROM THE COMMAND LINE ===================================================================
while getopts "a:eg:hm:n:o:p:s:" options; do
  case $options in
    a ) alt_hypoth=$OPTARG;;
    e ) echo_prefix="echo";;
    g ) gene_hypoth=$OPTARG;;
    h ) usage
          exit 1;;
    m ) metadata_file=$OPTARG;;
    n ) null_hypoth=$OPTARG;;
    o ) output_dir=$OPTARG;;
    p ) phase=$OPTARG;;
    s ) effect_hypoth=$OPTARG;;
    \? ) usage
          exit 1;;
    * ) usage
          exit 1;;
  esac
done
shift $((OPTIND - 1))

# ================= EXTRACT VARIABLES FROM THE CONFIG FILE ====================================================================
config_file=${HOME}/.config.ini

# ... directory containing ENSEMBL ID to HGNC ID dictionary
dict_dir=`parse_config ${config_file} system dict_dir`

# ... full path to private build of FusionInspector
fusion_inspector_exec=`parse_config ${config_file} STAR-Fusion fusion_inspector_exec`

# ... gtf file
gtf_file=`parse_config ${config_file} alignment macaque_virus_gtf_file`

# ... human to macaque orthologue file
ortholog_file=`parse_config ${config_file} alignment ortholog_file`

# ... QoRTs jar directory
qorts_jar_dir=`parse_config ${config_file} QoRTs qorts_jar_dir`

# ... QoRTs scripts directory
qorts_scripts_dir=`parse_config ${config_file} QoRTs qorts_scripts_dir`

# ... R scripts directory
R_scripts_dir=`parse_config ${config_file} STAR-Fusion R_scripts_dir`

# ... samtools executable
samtools=`parse_config ${config_file} alignment sam_tools`

# ... macaque STAR-Fusion index directory - N.B. use this for all calls of STAR-Fusion
star_fusion_index_dir=`parse_config ${config_file} STAR-Fusion macaque_virus_index`

# ... macaque STAR index directory - N.B. use this for all calls of STAR
star_index_dir=`parse_config ${config_file} STAR macaque_virus_index`

# ... full path to private build of STAR aligner
star_exec=`parse_config ${config_file} STAR star_exec`

# ... full path to private build of STAR-Fusion
star_fusion_exec=`parse_config ${config_file} STAR-Fusion star_fusion_exec`

# ================= BEGINNING TO GET SERIOUS ABOUT ACTUALLY RUNNING ===========================================================
echo -e
echo -e "\e[1;96mThis is ${programname}, a set of scripts to compute differential expression of splice variants and discover gene fusions from RNA-seq data\e[0m"

# ================= Define temporary and result directories ===================================================================
# ... alignment base directory
alignment_base_dir=${output_dir}/alignments

# ... FASTQ file directory
fastq_dir=${output_dir}/FASTQ

# ... log file directory
log_dir=${output_dir}/log

# ... QC directory
qc_dir=${output_dir}/QC

# ... results directory
results_dir=${output_dir}/results

# ... STAR-Fusion results directory
star_fusion_results_dir=${output_dir}/STAR-Fusion_results

# ... summary PDFs directory
summaryPDFs=${output_dir}/summaryPDFs

# ... temporary directory
tmp_dir=${output_dir}/tmp

# ================= PHASE 0: Copy FASTQ's to temporary location ==============================================================
if (( ${phase} <= 0 ))
then
  # ... Parse the metadata file
  # ... ... Extract header
  n_fields=`head -n 1 ${metadata_file} | wc -w`

  # ... ... Compute number of samples
  n_samples=`tail -n +2 ${metadata_file} | wc -l`

  for i in $(seq 1 ${n_fields})
  do
    i_th_element=`head -n 1 ${metadata_file} | cut -f ${i}`
    case ${i_th_element} in
      cl*|Cl*|CL*)
        clean_names=`tail -n +2 ${metadata_file} | cut -f ${i}`
        ;;
      pa*|Pa*|PA*)
        paths=`tail -n +2 ${metadata_file} | cut -f ${i}`
        ;;
      sample.id|Sample.id|sample.ID|Sample.ID)
        sample_ids=`tail -n +2 ${metadata_file} | cut -f ${i}`
        ;;
    esac
  done

  # ... Make any needed directories
  # ... ... base directory for all alignment results
  mkdir -p ${alignment_base_dir}

  # ... ... temporary directory to which all FASTQs are copied
  mkdir -p ${fastq_dir}

  # ... ... directory for all log files
  mkdir -p ${log_dir}

  # ... ... directory in which all on-the-fly shell scripts are assembled
  mkdir -p ${tmp_dir}

  # ... Copy FASTQ to output directory
  module load parallel

  for i in $(seq 1 ${n_samples})
  do
    clean_name=`echo ${clean_names[@]} | cut -d ' ' -f ${i}`
    path=`echo ${paths[@]} | cut -d ' '  -f ${i}`
    sample=`echo ${sample_ids[@]} | cut -d ' ' -f ${i}`

    cp_r1_cmd="sem -j ${SLURM_CPUS_PER_TASK} cp ${path}/${sample}_R1_001.fastq.gz ${fastq_dir}/${sample}_R1_001.fastq.gz"
    cp_r2_cmd="sem -j ${SLURM_CPUS_PER_TASK} cp ${path}/${sample}_R2_001.fastq.gz ${fastq_dir}/${sample}_R2_001.fastq.gz"

    # ... Copy the FASTQ's
    echo ${cp_r1_cmd}
    ${echo_prefix} ${cp_r1_cmd}
    echo ${cp_r2_cmd}
    ${echo_prefix} ${cp_r2_cmd}
  done

  sem --semaphoretimeout 7200

  module unload parallel
  echo -e ""
fi

# ================= PHASE 1: Align via STAR in swarm ==========================================================================
# Determine file names of all samples
fastq_files=`ls ${fastq_dir}/*.fastq.gz | sed "s/_R[1-2]//g" | sed "s/_001//g" | sed "s/.fastq.gz//g" | uniq`

if (( ${phase} <= 1 ))
then
  echo -e "\e[1;31mLaunching swarm to align FASTQ files ...\e[0m"
  # ... Load STAR environment - or use private build
  # module load STAR

  # ... basic STAR aligner command - N.B. important to use the ${star_index_dir} here
  star_align="${star_exec} --genomeDir ${star_index_dir} --runThreadN ${SLURM_CPUS_PER_TASK} --readFilesCommand zcat"

  # ... STAR flags recommended for downstream STAR-fusion
  star_fusion_flags="--twopassMode Basic --outReadsUnmapped None --chimSegmentMin 12 --chimJunctionOverhangMin 12 --alignSJDBoverhangMin 10 --alignMatesGapMax 100000 --alignIntronMax 100000 -chimSegmentReadGapMax 3 --alignSJstitchMismatchNmax 5 -1 5 5 --outSAMstrandField intronMotif"

  # ... Initialize swarm command file
  echo -n "" > ${tmp_dir}/align_with_STAR.swarm
  for f in ${fastq_files[@]}
  do
    # ... Make sub-directory for alignment output
    mkdir -p ${alignment_base_dir}/$(basename ${f})

    star_cmd="${star_align} --readFilesIn $fastq_dir/$(basename ${f})_R1_001.fastq.gz $fastq_dir/$(basename ${f})_R2_001.fastq.gz --outFileNamePrefix ${alignment_base_dir}/$(basename ${f})/$(basename ${f}). ${star_fusion_flags}"
    echo "cd ${alignment_base_dir}/$(basename ${f}); ${star_cmd}" >> ${tmp_dir}/align_with_STAR.swarm
  done

  # ... Launch the swarm
  swarm_cmd="swarm -f ${tmp_dir}/align_with_STAR.swarm -g 120 -t ${SLURM_CPUS_PER_TASK} --logdir ${log_dir} --time 24:00:00"
  echo ${swarm_cmd}
  dependency_1=`${echo_prefix} ${swarm_cmd}`

  # ... Unload STAR environment, if previously loaded
  # module unload STAR
  echo -e "\e[1;31mLaunched swarm for STAR alignments.\e[0m"
  echo -e ""
fi

# ================= PHASE 2: Run STAR-Fusion and QoRTs =========================================================================
if (( ${phase} <= 2 ))
then
  echo -e "\e[1;31mLaunching STAR-Fusion and QoRTs swarms ...\e[0m"
  # ... Make sub-directory for results
  mkdir -p ${star_fusion_results_dir}

  echo -e "\e[1;31m... Launching STAR-Fusion swarm ...\e[0m"

  # ... Run STAR-Fusion on the chimeric junction file that was created by the STAR aligner
  # ... ... Basic STAR-Fusion command
  star_fusion="${star_fusion_exec} --genome_lib_dir ${star_fusion_index_dir}"

  # ... ... Initialize the swarm command file
  echo -n "" > ${tmp_dir}/STAR-Fusion.swarm
  for f in ${fastq_files[@]}
  do
    # ... Make sub-directory for STAR-fusion results
    mkdir -p ${star_fusion_results_dir}/$(basename ${f})

    star_fusion_cmd="${star_fusion} --left_fq $fastq_dir/$(basename ${f})_R1_001.fastq.gz --right_fq $fastq_dir/$(basename ${f})_R2_001.fastq.gz -J ${alignment_base_dir}/$(basename ${f})/$(basename ${f}).Chimeric.out.junction --output_dir ${star_fusion_results_dir}/$(basename ${f})"
    echo "cd ${star_fusion_results_dir}/$(basename ${f}); ${star_fusion_cmd}" >> ${tmp_dir}/STAR-Fusion.swarm
  done

  # ... Final preparations to launch the swarm
  swarm_cmd="-f ${tmp_dir}/STAR-Fusion.swarm -g 120 -t ${SLURM_CPUS_PER_TASK} --logdir ${log_dir} --module perl,samtools --time 12:00:00"
  if (( ${phase} <= 1 ))
  then
    # ... Wait for the preceding dependencies to finish
    swarm_dependency="--dependency=afterok:${dependency_1}"
  else
    # ... There are no preceding dependencies, launch the swarm
    swarm_dependency=""
  fi

  # ... Launch the swarm
  echo "swarm ${swarm_dependency} ${swarm_cmd}"
  dependency_2=`${echo_prefix} swarm ${swarm_dependency} ${swarm_cmd}`

  echo -e "\e[1;31m... Completed launching STAR-Fusion swarm.\e[0m"

  # ... Convert SAM to BAM (for QoRTs)
  echo -e "\e[1;31m... Launching samtools swarm to convert SAM files into BAM files.\e[0m"
  
  # ... ... Initialize the swarm command file 
  echo -n "" > ${tmp_dir}/samtools.swarm
  for f in ${fastq_files[@]}
  do
    sam_in=$(basename ${f}).Aligned.out.sam
    bam_out=$(basename ${f}).bam
    samtools_cmd="${samtools} view -bS ${alignment_base_dir}/$(basename ${f})/${sam_in} -o ${alignment_base_dir}/$(basename ${f})/${bam_out}"
    echo "cd ${alignment_base_dir}/$(basename ${f}); ${samtools_cmd}" >> ${tmp_dir}/samtools.swarm
  done

  # ... Final preparations to launch the swarm
  swarm_cmd="-f ${tmp_dir}/samtools.swarm -g 120 -t ${SLURM_CPUS_PER_TASK} --logdir ${log_dir} --module samtools --time 12:00:00"

  # ... Launch the swarm
  echo "swarm ${swarm_dependency} ${swarm_cmd}" 
  dependency_3=`${echo_prefix} swarm ${swarm_dependency} ${swarm_cmd}`

  echo -e "\e[1;31m... Completed launching samtools swarm for conversion of SAM to BAM files.\e[0m"

  echo -e "\e[1;31mCompleted launching STAR-Fusion and samtools swarms.\e[0m"
  echo -e ""
fi

# ================= PHASE 3: Generate read counts =============================================================================
if (( ${phase} <= 3 ))
then
  # ... FusionInspector here
  echo -e "\e[1;31mUsing FusionInspector to validate putative fusion genes ...\e[0m"
  fusion_inspector="${fusion_inspector_exec} --genome_lib_dir ${star_fusion_index_dir}"

  echo -n "" > ${tmp_dir}/fusion_inspector.swarm
  for f in ${fastq_files[@]}
  do
    echo "Processing $(basename ${f}) ..."

    fusion_inspector_cmd="${fusion_inspector} --CPU ${SLURM_CPUS_PER_TASK} --fusions ${tmp_dir}/fusions.$(basename ${f}).txt --left_fq ${fastq_dir}/$(basename ${f})_R1_001.fastq.gz --right_fq ${fastq_dir}/$(basename ${f})_R2_001.fastq.gz --out_dir ${star_fusion_results_dir}/$(basename ${f}) --out_prefix finspector --prep_for_IGV"
    echo "cd ${star_fusion_results_dir}/$(basename ${f}); tail -n +1 ${star_fusion_results_dir}/$(basename ${f})/star-fusion.fusion_predictions.tsv | cut -f 1 >> ${tmp_dir}/fusions.$(basename ${f}).txt; ${fusion_inspector_cmd}" >> ${tmp_dir}/fusion_inspector.swarm
  done

  # ... Final preparations to launch the swarm
  swarm_cmd="-f ${tmp_dir}/fusion_inspector.swarm -g 120 -t ${SLURM_CPUS_PER_TASK} --logdir ${log_dir} --module perl,samtools,trinity --time 24:00:00"
  if (( ${phase} <= 2 ))
  then
    # ... Wait for the preceding dependencies to finish
    swarm_dependency="--dependency=afterok:${dependency_2}"
  else
    # ... There are no preceding dependencies, launch the swarm
    swarm_dependency=""
  fi

  # ... Launch the swarm
  echo "swarm ${swarm_dependency} ${swarm_cmd}" 
  dependency_3_1=`${echo_prefix} swarm ${swarm_dependency} ${swarm_cmd}`

  echo -e "\e[1;31mQuality Control using QoRTs ...\e[0m"

  # ... Initialize the swarm command file
  echo -n "" > ${tmp_dir}/qorts.swarm
  for f in ${fastq_files[@]}
  do
    bam_in=$(basename ${f}).bam

    # ... Create sub-directory for the QC and raw counts
    mkdir -p  ${qc_dir}/$(basename ${f})

    read_count_cmd="cd ${qc_dir}/$(basename ${f}); java -Xmx20G -jar ${qorts_jar_dir}/QoRTs.jar QC --stranded --chromSizes ${star_index_dir}/chrNameLength.txt ${alignment_base_dir}/$(basename ${f})/${bam_in} ${gtf_file} ${qc_dir}/$(basename ${f})"
    echo ${read_count_cmd} >>  ${tmp_dir}/qorts.swarm
  done

  # ... Final preparations to launch the swarm
  swarm_cmd="-f ${tmp_dir}/qorts.swarm -g 120 -t ${SLURM_CPUS_PER_TASK} --logdir ${log_dir} --module java,QoRTs --time 12:00:00"
  if (( ${phase} <= 2 ))
  then
    # ... Wait for the preceding dependencies to finish
    swarm_dependency="--dependency=afterok:${dependency_3}"
  else
    # ... There are no preceding dependencies, launch the swarm
    swarm_dependency=""
  fi

  # ... Launch the swarm
  echo "swarm ${swarm_dependency} ${swarm_cmd}"
  dependency_4=`${echo_prefix} swarm ${swarm_dependency} ${swarm_cmd}`

  echo -e "\e[1;31mLaunched swarm to generate read count files.\e[0m"
fi

# ================== PHASE 4: Run R quality control process with size factors =================================================
if (( ${phase} <= 4 ))
then
  # ... First use of summaryPDFs directory, so create it
  mkdir -p ${summaryPDFs}

  echo -e "\e[1;31mRunning QC, computing size factors and performing differential gene expression analysis ... \e[0m"

  # ... Assemble R scripts
  # ... ... splice variant analysis script
  cat ${R_scripts_dir}/differential.splicevariant.analysis.R > ${tmp_dir}/run.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.splicevariant.analysis.R
  cat ${R_scripts_dir}/utilities.R >> ${tmp_dir}/run.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.splicevariant.analysis.R
  echo "x <- generate.qc.plots(metadata.file = \"${metadata_file}\", base.results.dir = \"${output_dir}\", alpha = 0.05)" >> ${tmp_dir}/run.splicevariant.analysis.R

  # ... ... differential splice variant analysis script
  cat ${R_scripts_dir}/annotate.variantsplicing.results.R > ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/differential.splicevariant.analysis.R > ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/reorder.flatGffData.R >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run_differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/rename.jscs.R >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/utilities.R >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/annotate.variantsplicing.results.R >> ${tmp_dir}/run_differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run_differential.splice.variant.analysis.R
  echo "x <- differential.splicevariant.expression(metadata.file = \"${metadata_file}\", base.results.dir = \"${output_dir}\", alpha = 0.05, n.cores = ${SLURM_CPUS_PER_TASK}, dict.dir = \"${dict_dir}\", method.GLM = \"advanced\", test.formula0 = as.formula(\"${null_hypoth}\"), test.formula1 = as.formula(\"${alt_hypoth}\"), effect.formula = as.formula(\"${effect_hypoth}\"), geneLevel.formula = as.formula(\"${gene_hypoth}\"), ortholog.file = \"${ortholog_file}\", species = \"macaque\")" >> ${tmp_dir}/run.differential.splicevariant.analysis.R

  # ... Assemble driver shell script
  # ... ... top matter
  echo "#!/bin/bash" > ${tmp_dir}/run_splice_variant_analysis.sh
  echo "" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "# Ensure that the entire pipeline fails, if it encouters an error at an intermediate step" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "set -o pipefail" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "set -e" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "# ... Load required modules" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "module load java" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "module load QoRTs" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "module load R" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "" >> ${tmp_dir}/run_splice_variant_analysis.sh

  # ... ... body 
  # ... ... ... splice variant analysis
  echo "" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "# Compute splice variant analysis" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "R --vanilla < ${tmp_dir}/run.splicevariant.analysis.R" >> ${tmp_dir}/run_splice_variant_analysis.sh
  # echo "Rscript --vanilla ${qorts_scripts_dir}/qortsGenMultiQC.R ${qc_dir}/ ${metadata_file} ${summaryPDFs}/ >> ${log_dir}/run.splicevariant.analysis.log.txt" >> ${tmp_dir}/run_splice_variant_analysis.sh # Appears to be redundant

  # ... ... ... add novel splice junctions
  echo "" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "# Add novel splice junctions" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "java -Xmx20G -jar ${qorts_jar_dir}/QoRTs.jar mergeNovelSplices --minCount 2 --stranded ${qc_dir} ${qc_dir}/sizeFactors.GEO.txt ${gtf_file} ${qc_dir}" >> ${tmp_dir}/run_splice_variant_analysis.sh

  # ... ... ... differential splice variant analysis
  echo "" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "# Compute differential splice variant analysis" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "R --vanilla < ${tmp_dir}/run.differential.splicevariant.analysis.R" >> ${tmp_dir}/run_splice_variant_analysis.sh

  # ... ... closing
  echo "" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "module unload java" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "module unload QoRTs" >> ${tmp_dir}/run_splice_variant_analysis.sh
  echo "module unload R" >> ${tmp_dir}/run_splice_variant_analysis.sh

  # ... Command to launch the batch script
  sbatch_cmd="--cpus-per-task=${SLURM_CPUS_PER_TASK} --partition=norm --mem=200g --time=12:00:00 ${tmp_dir}/run_splice_variant_analysis.sh"
  if (( ${phase} <= 3 ))
  then
    # ... Wait for the preceding dependencies to finish
    sbatch_dependency="--dependency=afterok:${dependency_4}"
  else
    # ... There are no preceding dependencies, launch the batch job
    sbatch_dependency=""
  fi

  # ... Launch the batch script
  echo "sbatch  ${sbatch_dependency} ${sbatch_cmd}"
  ${echo_prefix} sbatch ${sbatch_dependency} ${sbatch_cmd}

  echo -e "\e[1;31mCompleted running QC, computing size factors and performing differential gene expression analysis.\e[0m"
  echo -e ""
fi

# ================== PHASE 5: Add novel splice junctions ======================================================================
if (( ${phase} == 5 ))
then
  # Note we skip this step if PHASE 4, because was run with the previous step
  echo -e "\e[1;31mComputing novel splice junctions ... \e[0m"

  # ... Assemble the differential splice variant analysis script
  cat ${R_scripts_dir}/annotate.variantsplicing.results.R > ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/differential.splicevariant.analysis.R > ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/reorder.flatGffData.R >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run_differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/rename.jscs.R >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/utilities.R >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/annotate.variantsplicing.results.R >> ${tmp_dir}/run_differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run_differential.splice.variant.analysis.R
  echo "x <- differential.splicevariant.expression(metadata.file = \"${metadata_file}\", base.results.dir = \"${output_dir}\", alpha = 0.05, n.cores = ${SLURM_CPUS_PER_TASK}, dict.dir = \"${dict_dir}\", method.GLM = \"advanced\", test.formula0 = as.formula(\"${null_hypoth}\"), test.formula1 = as.formula(\"${alt_hypoth}\"), effect.formula = as.formula(\"${effect_hypoth}\"), geneLevel.formula = as.formula(\"${gene_hypoth}\"), ortholog.file = \"${ortholog_file}\", species = \"macaque\")" >> ${tmp_dir}/run.differential.splicevariant.analysis.R

  # ... Assemble the driver script
  echo "#!/bin/bash" > ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "# Ensure that the entire pipeline fails, if it encouters an error at an intermediate step" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "set -o pipefail" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "set -e" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "# ... Load required modules" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "module load java" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "module load QoRTs" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "module load R" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "" >> ${tmp_dir}/run_add_novel_splice_junctions.sh

  # ... ... body
  # ... ... ... add novel splice junctions
  echo "java -Xmx20G -jar ${qorts_jar_dir}/QoRTs.jar mergeNovelSplices --minCount 2 --stranded ${qc_dir} ${qc_dir}/sizeFactors.GEO.txt ${gtf_file} ${qc_dir}" >> ${tmp_dir}/run_add_novel_splice_junctions.sh

  # ... ... ... differential splice variant analysis
  echo "R --vanilla < ${tmp_dir}/run.differential.splicevariant.analysis.R" >> ${tmp_dir}/run_add_novel_splice_junctions.sh

  # ... ... closing
  echo "" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "module unload java" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "module unload QoRTs" >> ${tmp_dir}/run_add_novel_splice_junctions.sh
  echo "module unload R" >> ${tmp_dir}/run_add_novel_splice_junctions.sh

  # ... Assemble command to launch the batch script
  sbatch_cmd="--cpus-per-task=${SLURM_CPUS_PER_TASK} --partition=norm --mem=200g --time=12:00:00 ${tmp_dir}/run_add_novel_splice_junctions.sh"

  # ... Launch the batch script
  echo "sbatch ${sbatch_cmd}"
  ${echo_prefix} sbatch ${sbatch_cmd}

  echo -e "\e[1;31mCompleted the addition of novel junctions.\e[0m"
  echo -e ""
fi

# ================== PHASE 6: Count novel splice junctions =============================================================
if (( ${phase} == 6 ))
then
  # Note we skip this step if PHASE 4 or 5, because was run with the previous step
  echo -e "\e[1;31mRunning JunctionSeq to compute hybrid differential expression of exons and splice junctions ... \e[0m"

# ... Assemble the differential splice variant analysis script
  cat ${R_scripts_dir}/annotate.variantsplicing.results.R > ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/differential.splicevariant.analysis.R > ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/reorder.flatGffData.R >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run_differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/rename.jscs.R >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/utilities.R >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run.differential.splicevariant.analysis.R
  cat ${R_scripts_dir}/annotate.variantsplicing.results.R >> ${tmp_dir}/run_differential.splicevariant.analysis.R
  echo "" >> ${tmp_dir}/run_differential.splice.variant.analysis.R
  echo "x <- differential.splicevariant.expression(metadata.file = \"${metadata_file}\", base.results.dir = \"${output_dir}\", alpha = 0.05, n.cores = ${SLURM_CPUS_PER_TASK}, dict.dir = \"${dict_dir}\", method.GLM = \"advanced\", test.formula0 = as.formula(\"${null_hypoth}\"), test.formula1 = as.formula(\"${alt_hypoth}\"), effect.formula = as.formula(\"${effect_hypoth}\"), geneLevel.formula = as.formula(\"${gene_hypoth}\"), ortholog.file = \"${ortholog_file}\", species = \"macaque\")" >> ${tmp_dir}/run.differential.splicevariant.analysis.R

  # ... Assemble the driver script
  echo "#!/bin/bash" > ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "# Ensure that the entire pipeline fails, if it encouters an error at an intermediate step" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "set -o pipefail" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "set -e" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "# ... Load required modules" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "module load R" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh

  echo "R --vanilla < ${tmp_dir}/run.differential.splicevariant.analysis.R" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh
  echo "module unload R" >> ${tmp_dir}/run_differential_splice_variant_analysis.sh

  sbatch_cmd="--cpus-per-task=${SLURM_CPUS_PER_TASK} --partition=norm --mem=200g --time=12:00:00 ${tmp_dir}/run_differential_splice_variant_analysis.sh"

  # ... Launch the batch script
  echo "sbatch ${sbatch_cmd}"
  ${echo_prefix} sbatch ${sbatch_cmd}

  echo -e "\e[1;31mCompleted running JunctionSeq to compute hybrid differential expression of exons and splice junctions ... \e[0m"
  echo -e ""
fi

# ================== PHASE 7: Generate browser tracks ==================================================================
if (( ${phase} <= 7 ))
then
  echo -e "\e[1;31mGenerating browser tracks ... \e[0m"
  echo -e "\e[1;31mNot yet implemented.\e[0m"
fi
