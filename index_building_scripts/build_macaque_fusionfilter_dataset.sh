#!/bin/bash

# Ensure that entire pipeline fails, if it encounters an error at an intermediate step
set -o pipefail
set -e

# ===================== DEFINE SOME CONVENIENCE FUNCTIONS ======================================================================
# ... Function to print informative error message upon fail
function fail {
        echo "$@" >&2
        exit 1
}

# ... Function to parse variables from configuration file
function parse_config () {
    awk -v section="$2" -v variable="$3" '
        $0 == "[" section "]" { in_section = 1; next }
        in_section && $1 == variable {
            $1=""
            $2=""
            sub(/^[[:space:]]+/, "")
            print
            exit 
        }
        in_section && $1 == "" {
            # we are at a blank line without finding the var in the section
            print "Variable not found in configuration file." > "/dev/stderr"
            exit 1
        }
    ' "$1"
}

echo "Make sure you have first generated STAR index using our script - this also generates the combined GTF and FASTA files."

# ================== EXTRACT VARIABLES FROM THE CONFIG FILE ===================================================================
config_file=$HOME/.config.ini

# ... Combined macaque and viral reference files
combined_gtf=`parse_config ${config_file} alignment macaque_virus_gtf_file`
combined_ref_genome=`parse_config ${config_file} alignment macaque_virus_reference`

# ... PFAM DB
pfam_db=`parse_config ${config_file} PFAM pfam_db`

# ... STAR-Fusion and FusionFilter binary files
fusion_filter_script=`parse_config ${config_file} STAR-Fusion fusion_filter_script`

# ... STAR-Fusion reference genome
sf_index_dir=`parse_config ${config_file} STAR-Fusion macaque_virus_index`

module load blast
module load hmmer
module load perl
module load samtools

# Explicitly add private build of STAR (V 2.6.0a) to the path.
export PATH=/data/DunbarGroup/src/STAR-2.6.0a/bin:$PATH

sf_cmd="perl ${fusion_filter_script} --genome_fa ${combined_ref_genome} --gtf ${combined_gtf} --output_dir ${sf_index_dir} --pfam_db ${pfam_db} --CPU ${SLURM_CPUS_PER_TASK}"
echo ${sf_cmd}
${sf_cmd}

module unload blast
module unload hmmer
module unload perl
module unload samtools
