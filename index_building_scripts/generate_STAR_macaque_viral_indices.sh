#! /bin/bash
# SBATCH --partition=norm --cpus-per-task=8 --mem=200g
# This file is generate_STAR_indices.sh

# Ensure that entire pipeline fails, if it encounters an error at an intermediate step
set -o pipefail
set -e

# ===================== DEFINE SOME CONVENIENCE FUNCTIONS ======================================================================
# ... Function to print informative error message upon fail
function fail {
        echo "$@" >&2
        exit 1
}

# ... Function to parse variables from configuration file
function parse_config () {
    awk -v section="$2" -v variable="$3" '
        $0 == "[" section "]" { in_section = 1; next }
        in_section && $1 == variable {
            $1=""
            $2=""
            sub(/^[[:space:]]+/, "")
            print
            exit 
        }
        in_section && $1 == "" {
            # we are at a blank line without finding the var in the section
            print "Variable not found in configuration file." > "/dev/stderr"
            exit 1
        }
    ' "$1"
}

# ================= EXTRACT VARIABLES FROM THE CONFIG FILE ====================================================================
config_file=$HOME/.config.ini

# ... macaque variables
macaque_gtf_file=`parse_config ${config_file} alignment macaque_gtf_file`
macaque_reference_fasta=`parse_config ${config_file} alignment macaque_reference`
macaque_virus_gtf_file=`parse_config ${config_file} alignment macaque_virus_gtf_file`
macaque_virus_index_dir=`parse_config ${config_file} STAR macaque_virus_index`
macaque_virus_reference_fasta=`parse_config ${config_file} alignment macaque_virus_reference`

# ... STAR binary - would use HPC module, but they lag considerably behind latest version
STAR=`parse_config ${config_file} STAR star_exec`

# ... viral variables
viral_gtf_file=`parse_config ${config_file} alignment viral_gtf_file`
viral_reference_fasta=`parse_config ${config_file} alignment viral_reference`

# ================= MAKE THE STAR INDEX DIRECTORY =============================================================================
mkdir -p ${macaque_virus_index_dir}

# ================= CONCATENATE GTF AND FASTA FILES ===========================================================================
# ... gtf
cat ${macaque_gtf_file} > ${macaque_virus_gtf_file}
tail -n +6  ${viral_gtf_file} >> ${macaque_virus_gtf_file}

# ... FASTA
cat ${macaque_reference_fasta} > ${macaque_virus_reference_fasta}
cat ${viral_reference_fasta} >> ${macaque_virus_reference_fasta}

# ================= CREATE INDICES ============================================================================================
star_cmd="${STAR} --runThreadN ${SLURM_CPUS_PER_TASK} --limitGenomeGenerateRAM 210000000000 --runMode genomeGenerate"

${star_cmd} --genomeDir ${macaque_virus_index_dir} --genomeFastaFiles ${macaque_virus_reference_fasta} --sjdbGTFfile ${macaque_virus_gtf_file} --sjdbOverhang 99
